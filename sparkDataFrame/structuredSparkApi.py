# -*- coding: utf-8 -*-
"""
Created on Wed Dec  4 19:53:07 2019

@author: mertcandurmus
"""

import findspark
findspark.init()
from pyspark.sql import SparkSession
from pyspark.sql import Row
from pyspark.sql.types import IntegerType
from pyspark.sql.functions import explode
from pyspark.sql.functions import split, col
from pyspark.sql.functions import desc
from pyspark.sql.functions import *
from pyspark.sql.types import StructType,StructField, StringType, IntegerType, FloatType

spark = SparkSession.builder.master("local[2]").appName("dataframe").config("spark.executor.memory","4g").config("spark.driver.memory","2g").getOrCreate()

sc = spark.sparkContext
#sc.stop()

#range ile dataframe oluşturma
df_frm_rng = sc.parallelize(range(1,100,5)).map(lambda x:(x,)).toDF(["range"])
df_frm_rng.show(3)

#dosyadan dataframe oluşturma
df_frm_file=spark.read.option("sep",";").option("header","True").option("inferSchema","True").csv("C:\\Users\\mert5\\Documents\\ApacheSparkWithPySpark\\pairRdd\\OnlineRetail.csv")
df_frm_file.show()
df_frm_file.count()


##word count örnegi stefan zweig  satranç 

hikaye=spark.read.text("C:\\Users\\mert5\\Documents\\ApacheSparkWithPySpark\\sparkDataFrame\\Satranç - Stefan Zweig ( PDFDrive.com ).txt")
hikaye.show(3,truncate=False)

kelimeler=hikaye.select(explode(split(col("value")," ")).alias("value"))
kelimeler.show(5)
kelimeler.groupBy("value").count().orderBy(desc("count")).show(35)


#csv üzerinde sql sorgusu çalıştırma

df_frm_file.createOrReplaceTempView("tablo")

spark.sql("""
          SELECT * FROM tablo

          """).show(4)

#veri temizleme ve veriyi tekrar kaydetme
#kullanılacak df df_frm_file
#initcap ile baş harfler büyük
df2=df_frm_file.withColumn("Description",trim(initcap(df_frm_file.Description)))

#temiz dataframei tekrar kaydetme
df2.coalesce(1).write.mode("append").option("sep",";").option("header","True").csv("C:\\Users\\mert5\\Documents\\ApacheSparkWithPySpark\\pairRdd\\OnlineRetail2.csv")


#schema oluşturmak

manual_schema = StructType(
[
    StructField("InvoiceNo", StringType(), True),
    StructField("StockCode", StringType(), True),
    StructField("Description", StringType(), True),
    StructField("Quantity", IntegerType(), True),
    StructField("InvoiceDate", StringType(), True),
    StructField("UnitPrice", FloatType(), True),
    StructField("CustomerID", IntegerType(), True),
    StructField("Country", StringType(), True)
]
)
#oluşturulan şema ile df tekrar okumak

df2 = spark.read \
.option("header","True") \
.schema(manual_schema) \
.option("sep",";") \
.csv("C:\\Users\\mert5\\Documents\\ApacheSparkWithPySpark\\pairRdd\\OnlineRetail2.csv")





















