# -*- coding: utf-8 -*-
"""
Created on Thu Dec  5 20:24:21 2019

@author: elifaskvav
"""

import findspark
findspark.init()
from pyspark.sql import SparkSession
from pyspark.sql.functions import *

spark = SparkSession.builder \
.master("local[4]") \
.appName("DataCleaning") \
.config("spark.executor.memory","4g") \
.config("spark.driver.memory","2g") \
.getOrCreate()



adult_train_df = spark.read.option("header","True").option("inferSchema","True").option("sep",",").csv("C:\\Users\\mert5\\Documents\\ApacheSparkWithPySpark\\machineLearningWİthSpark\\adult\\adult.data") 

adult_test_df = spark.read \
.option("header","True") \
.option("inferSchema","True") \
.option("sep",",") \
.csv("C:\\Users\\mert5\\Documents\\ApacheSparkWithPySpark\\machineLearningWİthSpark\\adult\\adult.test") \

#train ve test birleştirme

adult_whole_df = adult_train_df.union(adult_test_df)
adult_whole_df.limit(5).toPandas().head()

adult_whole_df1 = adult_whole_df \
.withColumn("workclass", trim(col("workclass"))) \
.withColumn("education", trim(col("education"))) \
.withColumn("marital_status", trim(col("marital_status"))) \
.withColumn("occupation", trim(col("occupation"))) \
.withColumn("relationship", trim(col("relationship"))) \
.withColumn("race", trim(col("race"))) \
.withColumn("sex", trim(col("sex"))) \
.withColumn("native_country", trim(col("native_country"))) \
.withColumn("output", trim(col("output"))) \




















