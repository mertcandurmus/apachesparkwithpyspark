# -*- coding: utf-8 -*-
"""
Created on Wed Dec  4 17:03:16 2019

@author: Mertcan Durmuş
"""

import findspark
findspark.init()
from pyspark.sql import SparkSession
from pyspark.conf import SparkConf

spark = SparkSession.builder.master("local[2]").appName("pairRdd").config("spark.executor.memory","4g").config("spark.driver.memory","2g").getOrCreate()

sc = spark.sparkContext

retailRDD=sc.textFile("C:\\Users\\mert5\\Documents\\ApacheSparkWithPySpark\\pairRdd\\OnlineRetail.csv")

#retailRDD.take(5)
firstLine=retailRDD.first()
firstLineRDD =sc.parallelize([firstLine])

retailRDDWithoutTitle=retailRDD.subtract(firstLineRDD)
#retailRDDWithoutTitle.take(5)

#birim miktarı 30 dan büyük olanlar filtreleniyor
retailRDDWithoutTitle.filter(lambda x: int(x.split(";")[3])>30).take(5)

#ürün adında coffee geçenler filtrelensin
retailRDDWithoutTitle.filter(lambda x: 'COFFEE' in x.split(";")[2]).take(5)

#iptal edilen satışların toplam ücreti

def cancelled(line):
    is_cancel=True if (line.split(";")[0].startswith("C")) else False
    quanity=float(line.split(";")[3])
    price=float(line.split(";")[5].replace(",","."))
    total=quanity*price
    return(is_cancel,total)


cancel_total=retailRDDWithoutTitle.map(cancelled)
cancel_total.take(5)
cancel_total_reduce=cancel_total.reduceByKey(lambda x,y:x+y)
cancel_total_reduce.take(5)






















    